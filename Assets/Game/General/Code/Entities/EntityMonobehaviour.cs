using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Self.Game
{
    public class EntityMonobehaviour : MonoBehaviour
    {
        // used for collision checking

        private GameEntity m_Entity;



        public virtual void Init(GameEntity entity)
        {
            m_Entity = entity;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            // some trigger logic?    
        }
    }
}
