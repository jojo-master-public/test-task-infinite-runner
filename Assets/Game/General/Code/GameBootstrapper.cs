using Leopotam.Ecs;
using UnityEngine;
using Self.Game.Configs;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;
using Self.Game.Systems;

namespace Self.Game
{
    public class GameBootstrapper : MonoBehaviour
    {
        private GameConfig m_GameConfig;
        private SystemsConfig m_SystemsConfig;
        private EcsWorld m_World;
        private EcsSystems m_Systems;

#if UNITY_EDITOR

        private List<IEcsSystem> m_ActiveSystems;
        public EcsSystems Systems => m_Systems;

#endif



        #region Unity Lifetime Events

        private void Awake()
        {
            // start up the game processes here!    
            LoadSettings();

            m_World = new EcsWorld();
            m_Systems = new EcsSystems(m_World, "Game World");

            var systems = CreateSystems();

            foreach (var sys in systems)
            {
                var sysName = sys is IEcsRunSystem 
                                ? sys.GetType().Name 
                                : null;

                m_Systems.Add(sys, sysName);
            }

            var systemsForInjection = systems.Where(s => s.GetType().GetCustomAttribute(typeof(InjectSystemAttribute)) != null);

            foreach (var sys in systemsForInjection)
            {
                m_Systems.Inject(sys);
            }

            // inject stuff here
            m_Systems.Inject(m_GameConfig)
                    .Inject(m_Systems);

            // basic init
            m_Systems.Init();
        }

        private void Update()
        {
            m_Systems.Run();
        }

        private void OnDestroy()
        {
            m_Systems.Destroy();
            m_World.Destroy();
        }

        #endregion

        #region Utility

        private void LoadSettings()
        {
            m_GameConfig = Resources.Load<GameConfig>("Settings/Config_Game");
            m_SystemsConfig = Resources.Load<SystemsConfig>("Settings/Config_Systems");
        }

        private List<IEcsSystem> CreateSystems()
        {
            var systemNamesToLoad = m_SystemsConfig.Systems;
            var currentAssembly = Assembly.GetAssembly(this.GetType());
            var ecsSystemTypes = currentAssembly.GetTypes()
                                                .Where(t => t.GetInterface(nameof(IEcsSystem)) != null)
                                                .ToArray();

            var systemsToLoad = new List<IEcsSystem>();

            for (int i = 0; i < systemNamesToLoad.Count; i++)
            {
                var ecsSystemType = ecsSystemTypes.FirstOrDefault(t => t.Name == systemNamesToLoad[i]);

                if (ecsSystemType == null)
                    continue;

                var newSystemInstance = (IEcsSystem)Activator.CreateInstance(ecsSystemType);

                systemsToLoad.Add(newSystemInstance);
            }

#if UNITY_EDITOR

            m_ActiveSystems = systemsToLoad;

#endif
            return systemsToLoad;
        }

        #endregion
    }
}