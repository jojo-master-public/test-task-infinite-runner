using DG.Tweening;
using UnityEngine;

namespace Self.Game.Configs
{
    // can be later replaced by json config if needed
    [CreateAssetMenu(fileName = "Settings_Game", menuName = "Game/Configs/General", order = 0)]
    public class GameConfig : ScriptableObject
    {
        [Header("Game")]
        public float GameResetTimer = 3f;

        // some settings here
        [Header("Obstacles")]
        public float MinObstacleSpawnDelay = 1.25f;
        public float MaxObstacleSpawnDelay = 2.5f;
        public float InitialObstacleSpawnDelay = 5f;
        public float ObstacleSpawnPositionY = 20f;
        public float ObstacleMovementSpeedMin = .1f;
        public float ObstacleMovementSpeedMax = .7f;
        public float ObstacleCollisionRadius = 2f;
        public float SceneBounds = -1f;
        // replace with string ids with links to assetbundles/addressables
        public Sprite[] ObstacleSprites;

        [Header("Player Movement")]
        public Ease PlayerMovementEase;
        public float PlayerMoveDuration;
        public float PlayerMoveInputThreshold;

        [Header("Player Positions")]
        public float PlayerInitialYPosition;
        public float[] PlayerPositions;
        public int PlayerInitialPosition = 1;

        [Header("Player Collision")]
        public float PlayerCollisionRadius = 1f;
        public float BackgroundScrollSpeed = 0.2f;
    }
}