using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using Leopotam.Ecs;
using System.Linq;
using Self.Game.Systems;

namespace Self.Game.Configs.Editors
{
    [CustomPropertyDrawer(typeof(SystemReference))]
    public class SystemReferencePropertyDrawer : PropertyDrawer
    {
        private List<string> m_CachedSystemNames;
        private SerializedProperty m_Property;
        private SerializedProperty m_SystemNameProperty;



        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            m_SystemNameProperty = property.FindPropertyRelative(nameof(SystemReference.SystemName));

            if (EditorGUI.DropdownButton(position, new GUIContent(m_SystemNameProperty.stringValue), FocusType.Passive))
                DrawDropDownMenu(property);
        }

        private void DrawDropDownMenu(SerializedProperty property)
        {
            if (m_CachedSystemNames == null)
                CacheSystemNames();

            m_SystemNameProperty = property.FindPropertyRelative(nameof(SystemReference.SystemName));

            var menu = new GenericMenu();

            foreach (var sys in m_CachedSystemNames)
            {
                menu.AddItem(new GUIContent(sys), sys == m_SystemNameProperty.stringValue, HandleSystemSelected, sys);
            }

            menu.ShowAsContext();
        }

        private void HandleSystemSelected(object systemName)
        {
            var convertedName = Convert.ToString(systemName);
            m_SystemNameProperty.stringValue = convertedName;
            m_SystemNameProperty.serializedObject.ApplyModifiedProperties();
        }

        private void CacheSystemNames()
        {
            m_CachedSystemNames = TypeCache.GetTypesDerivedFrom<IEcsSystem>()
                                            .Where(t => !t.IsAbstract && !t.IsInterface)
                                            .Where(t  => t.GetCustomAttribute(typeof(GameSystemAttribute)) != null)
                                            .Select(t => t.Name)
                                            .ToList();
        }
    }
}
