using System.Collections.Generic;
using UnityEngine;

namespace Self.Game.Configs
{
    [System.Serializable]
    public class SystemReference
    {
        public string SystemName;

        public static explicit operator string(SystemReference target) => target.SystemName;
        public static bool operator ==(SystemReference target, string name) => target.SystemName == name;
        public static bool operator !=(SystemReference target, string name) => target.SystemName != name;
        public static bool operator ==(string name, SystemReference target) => target.SystemName == name;
        public static bool operator !=(string name, SystemReference target) => target.SystemName != name;
    }

    [CreateAssetMenu(fileName = "Config_Systems", menuName = "Game/Configs/Systems", order = 0)]
    public class SystemsConfig : ScriptableObject
    {
        public List<SystemReference> Systems;
    }
}