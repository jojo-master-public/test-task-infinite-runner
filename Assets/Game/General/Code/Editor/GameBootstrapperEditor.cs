using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Leopotam.Ecs;
using System;
using System.Linq;

namespace Self.Game.Editors
{
    [CustomEditor(typeof(GameBootstrapper))]
    public class GameBootstrapperEditor : Editor
    {
        private GameBootstrapper m_BootStrapper;
        private EcsSystems m_Systems;



        private void OnEnable()
        {
            m_BootStrapper = serializedObject.targetObject as GameBootstrapper;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DrawInitSystems();
            DrawRunSystems();
            DrawDestroySystems();
        }

        private void DrawRunSystems()
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Active Run Systems", EditorStyles.boldLabel);

            if (m_Systems != null && m_Systems.GetRunSystems().Count > 0)
            {
                var runSystems = m_Systems.GetRunSystems(); 

                for (int i = 0; i < runSystems.Count; i++)
                {
                    var style = new GUIStyle(EditorStyles.label);
                    style.normal.textColor = runSystems.Items[i].Active
                                    ? new Color(0f, 1f, 0.2f, 1f) 
                                    : new Color(1f, 0.2f, 0.2f, 1f);

                    DrawSystemLabel(runSystems.Items[i].System, i, style);
                }
            }
            else
            {
                EditorGUILayout.LabelField("No Systems Loaded");
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawDestroySystems()
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Active Destroy Systems", EditorStyles.boldLabel);

            var systems = GetActiveSystems<IEcsDestroySystem>();

            if (systems != null && systems.Count > 0)
            {
                for (int i = 0; i < systems.Count; i++)
                {
                    DrawSystemLabel(systems[i], i);
                }
            }
            else
            {
                EditorGUILayout.LabelField("No Systems Loaded");
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawInitSystems()
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Active Init Systems", EditorStyles.boldLabel);

            var systems = GetActiveSystems<IEcsInitSystem>();

            if (systems != null && systems.Count > 0)
            {
                for (int i = 0; i < systems.Count; i++)
                {
                    DrawSystemLabel(systems[i], i);
                }
            }
            else
            {
                EditorGUILayout.LabelField("No Systems Loaded");
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawSystemLabel(IEcsSystem system, int index, GUIStyle style = null)
        {
            EditorGUILayout.BeginHorizontal();

            var systemType = system.GetType().Name;

            if(style != null)
                EditorGUILayout.LabelField(new GUIContent($"System_{index}"), new GUIContent(systemType), style);
            else
                EditorGUILayout.LabelField(new GUIContent($"System_{index}"), new GUIContent(systemType));

            EditorGUILayout.EndHorizontal();
        }

        private List<T> GetActiveSystems<T>() where T : class, IEcsSystem
        {
            if (m_Systems == null)
                m_Systems = m_BootStrapper.Systems;

            if(m_Systems != null)
            {
                List<T> systems = m_Systems.GetAllSystems()
                                                .Items
                                                .Where(s => s != null && s is T)
                                                .Select(s => s as T)
                                                .ToList();

                return systems;
            }

            return null;
        }
    }
}
