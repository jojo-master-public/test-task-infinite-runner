using DG.Tweening;
using Leopotam.Ecs;
using Self.Game.Components;
using Self.Game.Configs;
using UnityEngine;

namespace Self.Game.Systems
{
    [GameSystem]
    public class PlayerControllerSystem : IEcsRunSystem, IEcsInitSystem, IEcsDestroySystem
    {
        private GameConfig m_GameConfig;
        private EcsFilter<PlayerComponent, PositionComponent> m_Filter;
        private EcsWorld m_World;
        private Tweener m_PlayerMoveTweener;
        private Transform m_PlayerTransform;
        private int m_CurrentPlayerPosition;



        public void Init()
        {
            // init parameters here
            foreach (var playerIndex in m_Filter)
            {
                var playerComponent = m_Filter.Get1(playerIndex);
                m_PlayerTransform = playerComponent.PlayerTransform;
                m_CurrentPlayerPosition = m_GameConfig.PlayerInitialPosition;

                var initialPositionX = m_GameConfig.PlayerPositions[m_GameConfig.PlayerInitialPosition];
                var initialPositionY = m_GameConfig.PlayerInitialYPosition;

                m_PlayerTransform.position = new Vector3(initialPositionX, initialPositionY);

                var playerPosition = m_Filter.Get2(playerIndex);
                playerPosition.Position = m_PlayerTransform.position;
            }
        }

        public void Run()
        {
            // check for user input here
            var input = GetInput();
            // move player object only if input is present

            var inputThresholdCheck = Mathf.Abs(input.x) > m_GameConfig.PlayerMoveInputThreshold;

            // update player entity position
            foreach (var playerIndex in m_Filter)
            {
                ref var playerPosition = ref m_Filter.Get2(playerIndex);
                playerPosition.Position = m_PlayerTransform.position;
            }

            if (m_PlayerMoveTweener != null && m_PlayerMoveTweener.IsPlaying())
                return;

            if (inputThresholdCheck)
            {
                var direction = (int)Mathf.Sign(input.x);

                var oldPosition = m_CurrentPlayerPosition;

                m_CurrentPlayerPosition += direction;

                if (m_CurrentPlayerPosition >= m_GameConfig.PlayerPositions.Length)
                {
                    m_CurrentPlayerPosition = m_GameConfig.PlayerPositions.Length - 1;
                }
                else if (m_CurrentPlayerPosition < 0)
                {
                    m_CurrentPlayerPosition = 0;
                }

                if (oldPosition == m_CurrentPlayerPosition)
                    return;

                var nextPosition = m_GameConfig.PlayerPositions[m_CurrentPlayerPosition];

                m_PlayerMoveTweener = m_PlayerTransform.DOMoveX(nextPosition, m_GameConfig.PlayerMoveDuration)
                                                        .SetEase(m_GameConfig.PlayerMovementEase)
                                                        .SetAutoKill(false);
            }
        }

        public void Destroy()
        {
            m_PlayerMoveTweener = null;
            m_PlayerTransform = null;
        }

        private Vector2 GetInput()
        {
            if (Input.touchCount < 1)
            {
                if (Application.platform == RuntimePlatform.WindowsEditor
                    && Input.GetMouseButton(0))
                    return new Vector2(Input.GetAxis("Mouse X"), 0f);

                return Vector2.zero;
            }

            var input1 = Input.GetTouch(0);

            return input1.deltaPosition.normalized;
        }
    }
}
