using System;

namespace Self.Game.Systems
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectSystemAttribute : Attribute
    {

    }
}
