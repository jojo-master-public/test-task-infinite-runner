using System;

namespace Self.Game.Systems
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GameSystemAttribute : Attribute
    {
        // could be later used for
        // Systems Execution Order sorting at runtime
        public readonly int Order;

        public GameSystemAttribute(int order = -1)
        {
            this.Order = order;
        }
    }
}
