using Leopotam.Ecs;
using Self.Game.Components;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Self.Game.Systems
{
    public class CollisionResult
    {
        public int otherEntityIndex;
        public int entityIndex;
        public string otherEntityTag;
    }

    [GameSystem]
    public class EntityCollisionSystem : IEcsRunSystem
    {
        private EcsFilter<CollisionComponent, PositionComponent>
                    .Exclude<IsOutsideSceneComponent> m_Filter;



        public void Run()
        {
            var entitiesWithCollisions = new List<CollisionResult>();

            foreach (var entityIndex in m_Filter)
            {
                var entityPosition = m_Filter.Get2(entityIndex).Position;
                var entityRadius = m_Filter.Get1(entityIndex).Radius;
                var entityTag = m_Filter.Get1(entityIndex).Tag;

                foreach (var otherEntityIndex in m_Filter)
                {
                    if (entityIndex == otherEntityIndex)
                        continue;

                    var otherEntityPosition = m_Filter.Get2(otherEntityIndex).Position;
                    var otherEntityRadius = m_Filter.Get1(otherEntityIndex).Radius;
                    var otherEntityTag = m_Filter.Get1(otherEntityIndex).Tag;

                    var radiusSum = entityRadius + otherEntityRadius;
                    var distance = Vector2.Distance(entityPosition, otherEntityPosition);

                    if(radiusSum > distance && !entitiesWithCollisions.Any(cr => cr.entityIndex == entityIndex))
                    {
                        entitiesWithCollisions.Add(new CollisionResult
                        {
                            entityIndex = entityIndex,
                            otherEntityIndex = otherEntityIndex,
                            otherEntityTag = otherEntityTag
                        });
                    }
                }
            }

            foreach (var collisionResult in entitiesWithCollisions)
            {
                var entity = m_Filter.GetEntity(collisionResult.entityIndex);

                entity.Replace(new CollisionResultComponent()
                {
                    Tag = collisionResult.otherEntityTag
                });
            }
        }
    }
}
