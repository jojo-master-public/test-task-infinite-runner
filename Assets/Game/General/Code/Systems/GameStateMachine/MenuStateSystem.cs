using System.Collections.Generic;

namespace Self.Game.Systems.States
{
    [GameSystem]
    public class MenuStateSystem : GameStateSystemBase
    {
        protected override List<string> SystemsOnEnable => new List<string>
        {

        };

        public override void RunState()
        {

        }

        public override void OnStateEnter()
        {

        }

        public override void OnStateExit()
        {

        }
    }
}
