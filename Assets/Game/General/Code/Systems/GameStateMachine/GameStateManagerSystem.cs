using Leopotam.Ecs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Self.Game.Systems.States
{
    [GameSystem]
    [InjectSystem]
    public class GameStateManagerSystem : IEcsInitSystem, IEcsRunSystem
    {
        private List<string> m_GameStateSystemNames = new List<string>
        {
            typeof(MenuStateSystem).Name,
            typeof(GameplayStateSystem).Name,
            typeof(GameLoseStateSystem).Name
        };

        private EcsSystems m_SystemsContainer;
        private List<IGameStateSystem> m_GameStateSystems;
        private IGameStateSystem m_CurrentState;



        public void Init()
        {
            m_GameStateSystems = new List<IGameStateSystem>();

            var runSystems = m_SystemsContainer.GetRunSystems()
                                                .Items
                                                .Where(s => s != null && m_GameStateSystemNames.Contains(s.System.GetType().Name))
                                                .Select(s => s.System);

            foreach (var stateSystem in runSystems)
            {
                var state = stateSystem as IGameStateSystem;

                state.SetState(false);
                state.InitState();

                m_GameStateSystems.Add(stateSystem as IGameStateSystem);
            }

            var initGameStateSystems = m_GameStateSystems
                                            .Where(s => s is IEcsInitSystem)
                                            .Select(s => s as IEcsInitSystem);

            foreach (var sys in initGameStateSystems)
            {
                sys.Init();
            }

            m_CurrentState = runSystems.First(s => s.GetType().Name == nameof(GameplayStateSystem)) as IGameStateSystem;
            m_CurrentState.SetState(true);
        }

        public void Run()
        {
            m_CurrentState.RunState();
        }

        public void SetState(Type type)
        {
            var newState = m_GameStateSystems.FirstOrDefault(t => t.GetType() == type);

            if (newState == null)
                return;

            m_CurrentState.SetState(false);

            m_CurrentState = newState;

            m_CurrentState.SetState(true);
        }
    }
}
