﻿using Self.Game.Configs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Self.Game.Systems.States
{
    [GameSystem]
    [InjectSystem]
    public class GameLoseStateSystem : GameStateSystemBase
    {
        public event Action OnTimerEnded;

        private GameConfig m_Config;

        private float m_CurrentTimer = 0f;
        public float CurrentTimer => m_CurrentTimer;



        protected override List<string> SystemsOnEnable => new List<string> 
        { 
            nameof(UserInterfaceSystem),
            nameof(EntityCleanerSystem)
        };

        public override void OnStateEnter()
        {
            m_CurrentTimer = m_Config.GameResetTimer;
        }

        public override void RunState()
        {
            m_CurrentTimer -= Time.deltaTime;

            if(m_CurrentTimer <= 0f)
            {
                OnTimerEnded?.Invoke();

                m_StateManager.SetState(typeof(GameplayStateSystem));
            }
        }

        public override void OnStateExit()
        {

        }
    }
}