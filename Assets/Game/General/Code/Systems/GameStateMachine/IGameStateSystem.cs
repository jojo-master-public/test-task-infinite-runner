using Leopotam.Ecs;

namespace Self.Game.Systems.States
{
    public interface IGameStateSystem : IEcsRunSystem
    {
        bool IsEnabled { get; }

        void SetState(bool state);
        void OnStateEnter();
        void InitState();
        void RunState();
        void OnStateExit();
    }
}
