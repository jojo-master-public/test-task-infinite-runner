﻿using Leopotam.Ecs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Self.Game.Systems.States
{
    [GameSystem]
    public class GameplayStateSystem : GameStateSystemBase
    {
        protected override List<string> SystemsOnEnable => new List<string>
        {
            nameof(EntityMovementSystem),
            nameof(EntityCollisionResolverSystem),
            nameof(EntityCollisionSystem),
            nameof(ObstacleRenderSystem),
            nameof(ObstacleCheckInsideSceneSystem),
            nameof(ObstacleGeneratorSystem),
            nameof(PlayerControllerSystem)
        };

        private EntityCollisionResolverSystem m_CollisionResolver;



        public override void OnStateEnter()
        {
            m_CollisionResolver.OnPlayerCollisionEvent += HandleCollisionEvent;
        }

        public override void RunState()
        {

        }

        public override void OnStateExit()
        {
            m_CollisionResolver.OnPlayerCollisionEvent -= HandleCollisionEvent;
        }

        private void HandleCollisionEvent()
        {
            // something collided with the player

            m_StateManager.SetState(typeof(GameLoseStateSystem));
        }
    }
}
