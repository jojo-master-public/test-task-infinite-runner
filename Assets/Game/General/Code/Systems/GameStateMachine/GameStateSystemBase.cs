﻿using Leopotam.Ecs;
using System.Collections.Generic;

namespace Self.Game.Systems.States
{
    public abstract class GameStateSystemBase : IGameStateSystem
    {
        protected abstract List<string> SystemsOnEnable { get;}

        protected EcsWorld m_World;
        protected EcsSystems m_Systems;
        protected GameStateManagerSystem m_StateManager;

        public bool IsEnabled => m_IsEnabled;

        protected bool m_IsEnabled;



        public abstract void OnStateEnter();
        public abstract void RunState();
        public abstract void OnStateExit();

        public virtual void Run() { }
        public virtual void InitState() => SetSystemStates(false);
        public virtual void SetState(bool state) 
        {
            var oldState = m_IsEnabled;

            if(oldState && !state)
            {
                SetSystemStates(false);
                OnStateExit();
            }

            if(!oldState && state)
            {
                SetSystemStates(true);
                OnStateEnter();
            }

            m_IsEnabled = state; 
        }

        protected void SetSystemStates(bool state)
        {
            foreach (var sys in SystemsOnEnable)
            {
                var sysIndex = m_Systems.GetNamedRunSystem(sys);
                m_Systems.SetRunSystemState(sysIndex, state);
            }
        }
    }
}
