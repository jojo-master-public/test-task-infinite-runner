using Leopotam.Ecs;
using Self.Game.Components;

namespace Self.Game.Systems
{
    [GameSystem]
    public class EntityCleanerSystem : IEcsRunSystem
    {
        private EcsFilter<CollisionComponent> m_Filter;



        public void Run()
        {
            foreach (var entityIndex in m_Filter)
            {
                var entityTag = m_Filter.Get1(entityIndex).Tag;

                if(entityTag != "Player")
                {
                    var entity = m_Filter.GetEntity(entityIndex);

                    entity.Destroy();
                }
            }
        }
    }
}
