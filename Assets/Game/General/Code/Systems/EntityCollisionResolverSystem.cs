using Leopotam.Ecs;
using Self.Game.Components;
using System;

namespace Self.Game.Systems
{
    [GameSystem]
    [InjectSystem]
    public class EntityCollisionResolverSystem : IEcsRunSystem
    {
        public event Action OnPlayerCollisionEvent;
        public event Action OnCollisionEvent;

        private EcsFilter<CollisionResultComponent, CollisionComponent> m_Filter;



        public void Run()
        {
            if (m_Filter.GetEntitiesCount() == 0)
                return;

            // resolve collisions
            foreach (var entityIndex in m_Filter)
            {

            }

            // remove collision results
            foreach (var entityIndex in m_Filter)
            {
                var entity = m_Filter.GetEntity(entityIndex);
                var entityTag = m_Filter.Get2(entityIndex).Tag;

                if (entityTag == "Player")
                    OnPlayerCollisionEvent?.Invoke();

                entity.Del<CollisionResultComponent>();
            }

            OnCollisionEvent?.Invoke();
        }
    }
}
