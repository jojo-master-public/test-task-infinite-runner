using System.Linq;
using UnityEngine;
using Leopotam.Ecs;
using Self.Game.Configs;
using Self.Game.Components;

namespace Self.Game.Systems
{
    [GameSystem]
    public class BackgroundInitializerSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld m_World;
        private GameConfig m_Config;
        private Transform m_BackgroundTransform;

        private EcsFilter<PositionComponent, BackgroundComponent> m_Filter;


        public void Init()
        {
            var bg = GameObject.FindObjectsOfType<SpriteRenderer>().FirstOrDefault(bg => bg.name == "Background");

            if(bg != null)
            {
                m_BackgroundTransform = bg.transform;

                m_World.NewEntity()
                    .Replace(new BackgroundComponent())
                    .Replace(new MovementComponent() { Speed = m_Config.BackgroundScrollSpeed })
                    .Replace(new PositionComponent() { Position = m_BackgroundTransform.position });
            }
        }

        public void Run()
        {
            foreach (var backgroundIndex in m_Filter)
            {
                var position = m_Filter.Get1(backgroundIndex).Position;

                m_BackgroundTransform.position = position;
            }
        }
    }
}
