using Leopotam.Ecs;
using Self.Game.Components;
using Self.Game.Configs;
using System.Collections.Generic;
using UnityEngine;

namespace Self.Game.Systems
{
    [GameSystem]
    public class ObstacleGeneratorSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld m_World;
        private GameConfig m_Config;
        private float m_Timer = 0f;
        private float m_NextObstacleTime = 0f;

        private EcsFilter<IsOutsideSceneComponent> m_InactiveEntitiesFilter;



        public void Init()
        {
            m_NextObstacleTime = m_Config.InitialObstacleSpawnDelay;
        }

        public void Run()
        {
            if(m_Timer < m_NextObstacleTime)
            {
                m_Timer += Time.deltaTime;
            }
            else
            {
                m_NextObstacleTime = UnityEngine.Random.Range(m_Config.MinObstacleSpawnDelay, m_Config.MaxObstacleSpawnDelay);
                m_Timer = 0f;

                SpawnObstacle();
            }
        }

        private void SpawnObstacle()
        {
            var spawnPositionIndex = UnityEngine.Random.Range(0, m_Config.PlayerPositions.Length);
            var spawnPositionX = m_Config.PlayerPositions[spawnPositionIndex];
            var spawnPositionY = m_Config.ObstacleSpawnPositionY;

            var position = new Vector2(spawnPositionX, spawnPositionY);
            var movementSpeed = UnityEngine.Random.Range(m_Config.ObstacleMovementSpeedMin, m_Config.ObstacleMovementSpeedMax);

            GetEntity()
                .Replace(new PositionComponent { Position = position })
                .Replace(new MovementComponent { Speed = movementSpeed })
                .Replace(new CollisionComponent { Radius = m_Config.ObstacleCollisionRadius })
                .Replace(new SpriteComponent { Sprite = m_Config.ObstacleSprites[Random.Range(0, m_Config.ObstacleSprites.Length)] });
        }

        private EcsEntity GetEntity()
        {
            if(m_InactiveEntitiesFilter.GetEntitiesCount() > 0)
            {
                var entityToRemove = new List<int>();

                foreach (var entityIndex in m_InactiveEntitiesFilter)
                {
                    entityToRemove.Add(entityIndex);

                    break;
                }

                var entity = m_InactiveEntitiesFilter.GetEntity(entityToRemove[0]);

                entity.Del<IsOutsideSceneComponent>();

                return entity;
            }

            return m_World.NewEntity();
        }
    }
}
