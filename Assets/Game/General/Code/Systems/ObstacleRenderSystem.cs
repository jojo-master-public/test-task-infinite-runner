using System.Collections.Generic;
using UnityEngine;
using Self.Game.Components;
using Leopotam.Ecs;
using Self.Game.Configs;

namespace Self.Game.Systems
{
    [GameSystem]
    public class ObstacleRenderSystem : IEcsRunSystem, IEcsInitSystem, IEcsDestroySystem
    {
        private GameConfig m_Config;
        private List<SpriteRenderer> m_ActiveRenderers;
        private Stack<SpriteRenderer> m_RenderersPool;
        private EcsFilter<PositionComponent, SpriteComponent>.Exclude<IsOutsideSceneComponent> m_Filter;



        public void Init()
        {
            m_ActiveRenderers = new List<SpriteRenderer>();
            m_RenderersPool = new Stack<SpriteRenderer>();
        }

        public void Run()
        {
            ClearCurrentRenderers();
            RenderCurrentObstacles();
            HideExcessiveRenderers();
        }

        public void Destroy()
        {
            foreach (var r in m_ActiveRenderers)
            {
                if(r != null)
                    Object.Destroy(r.gameObject);
            }

            m_ActiveRenderers.Clear();
            m_ActiveRenderers = null;

            foreach (var r in m_RenderersPool)
            {
                if (r != null)
                    Object.Destroy(r.gameObject);
            }
            m_RenderersPool.Clear();
            m_RenderersPool = null;
        }

        // gets a renderer from pool
        // or creates new renderer
        private SpriteRenderer GetRenderer(int entityIndex)
        {
            if(m_RenderersPool.Count > 0)
            {
                var r = m_RenderersPool.Pop();
                r.name = $"Obstacle_[{entityIndex}]";

                return r;
            }
            else
            {
                var rendererObject = new GameObject($"Obstacle_[{entityIndex}]");
                rendererObject.transform.position = new Vector3(0, -15, 0);

                var renderer = rendererObject.AddComponent<SpriteRenderer>();

                return renderer;
            }
        }

        private void ClearCurrentRenderers()
        {
            var entitiesToRenderCount = m_Filter.GetEntitiesCount();

            foreach (var r in m_ActiveRenderers)
            {
                m_RenderersPool.Push(r);
            }

            m_ActiveRenderers.Clear();
            m_ActiveRenderers = new List<SpriteRenderer>(entitiesToRenderCount);
        }

        private void RenderCurrentObstacles()
        {
            // render current obstacles
            foreach (var entityIndex in m_Filter)
            {
                var entityPosition = m_Filter.Get1(entityIndex).Position;
                var entitySprite = m_Filter.Get2(entityIndex).Sprite;
                var renderer = GetRenderer(entityIndex);

                renderer.transform.position = entityPosition;
                renderer.sprite = entitySprite;
                renderer.gameObject.SetActive(true);

                m_ActiveRenderers.Add(renderer);
            }
        }

        private void HideExcessiveRenderers()
        {
            // hide game objects that are out of bounds
            foreach (var renderer in m_RenderersPool)
            {
                renderer.gameObject.SetActive(false);
            }
        }
    }
}
