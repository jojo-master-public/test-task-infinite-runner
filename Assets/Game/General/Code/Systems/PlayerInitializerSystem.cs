using Leopotam.Ecs;
using Self.Game.Components;
using UnityEngine;

namespace Self.Game.Systems
{
    [GameSystem]
    public class PlayerInitializerSystem : IEcsInitSystem
    {
        private EcsWorld m_World;



        public void Init()
        {
            var player = GameObject.FindWithTag("Player");

            m_World.NewEntity()
                .Replace(new PlayerComponent() { PlayerTransform = player.transform })
                .Replace(new PositionComponent() { Position = Vector2.zero })
                .Replace(new CollisionComponent() { Tag = "Player" });
        }
    }
}
