﻿using Leopotam.Ecs;
using System;
using System.Linq;
using TMPro;
using UnityEngine;
using DG.Tweening;
using Self.Game.Systems.States;

namespace Self.Game.Systems
{
    [GameSystem]
    public class UserInterfaceSystem : IEcsRunSystem, IEcsInitSystem, IEcsDestroySystem
    {
        public event Action OnTimerEnded;

        private GameLoseStateSystem m_LoseSystem;
        private CanvasGroup m_LoseCanvas;
        private TMP_Text m_TimerText;



        public void Init()
        {
            m_LoseCanvas = GameObject.FindObjectsOfType<CanvasGroup>().FirstOrDefault(c => c.name == "LoseCanvas");
            m_TimerText = GameObject.FindObjectsOfType<TMP_Text>().FirstOrDefault(t => t.name == "TimerText");

            m_LoseSystem.OnTimerEnded += OnLoseTimerEnded;
            m_LoseCanvas.DOFade(0f, 0.35f).SetEase(Ease.OutQuad);
        }

        public void Run()
        {
            m_LoseCanvas.alpha = 0.8f;
            m_TimerText.text = $"{m_LoseSystem.CurrentTimer.ToString("#")}";
        }

        private void OnLoseTimerEnded ()
        {
            m_LoseCanvas.DOFade(0f, 0.35f).SetEase(Ease.OutQuad);
        }

        public void Destroy()
        {
            if(m_LoseSystem != null)
                m_LoseSystem.OnTimerEnded -= OnLoseTimerEnded;
        }
    }
}