using Leopotam.Ecs;
using Self.Game.Components;
using Self.Game.Configs;
using System.Collections.Generic;

namespace Self.Game.Systems
{
    [GameSystem]
    public class ObstacleCheckInsideSceneSystem : IEcsRunSystem
    {
        private GameConfig m_Config;
        private EcsFilter<PositionComponent>.Exclude<IsOutsideSceneComponent> m_Filter;



        public void Run()
        {
            var entitiesOutsideSceneBounds = new List<int>();

            // collect entities that are out of bounds
            foreach (var entityIndex in m_Filter)
            {
                var entityPosition = m_Filter.Get1(entityIndex);

                if(entityPosition.Position.y < m_Config.SceneBounds)
                {
                    var index = entityIndex;
                    entitiesOutsideSceneBounds.Add(index);
                }
            }

            // process entities that are out of bounds
            foreach (var entityIndex in entitiesOutsideSceneBounds)
            {
                var entity = m_Filter.GetEntity(entityIndex);
                entity.Replace(new IsOutsideSceneComponent());
            }
        }
    }
}
