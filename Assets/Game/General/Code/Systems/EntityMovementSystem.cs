using Leopotam.Ecs;
using Self.Game.Components;
using System.Collections.Generic;
using UnityEngine;

namespace Self.Game.Systems
{
    public struct EntityMovementResult
    {
        public int entityIndex;
        public float movementDelta;
    }

    [GameSystem]
    public class EntityMovementSystem : IEcsRunSystem
    {
        private EcsFilter<MovementComponent, PositionComponent>
                    .Exclude<IsOutsideSceneComponent> m_Filter;



        public void Run()
        {
            var entitiesToMove = new List<EntityMovementResult>();

            // collect entities for movement
            foreach (var entityIndex in m_Filter)
            {
                var entityMovement = m_Filter.Get1(entityIndex).Speed;
                ref var entityPosition = ref m_Filter.Get2(entityIndex).Position;

                entitiesToMove.Add(new EntityMovementResult
                {
                    entityIndex = entityIndex,
                    movementDelta = -entityMovement * Time.deltaTime
                });
            }

            // move entities
            foreach (var entityMovement in entitiesToMove)
            {
                ref var entityPosition = ref m_Filter.Get2(entityMovement.entityIndex);
                entityPosition.Position.y += entityMovement.movementDelta;
            }
        }
    }
}
