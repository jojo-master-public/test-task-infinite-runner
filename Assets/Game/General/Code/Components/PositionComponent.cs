using UnityEngine;

namespace Self.Game.Components
{
    public struct PositionComponent
    {
        public Vector2 Position;
    }
}
