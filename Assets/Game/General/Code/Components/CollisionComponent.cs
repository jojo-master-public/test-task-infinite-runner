namespace Self.Game.Components
{
    public struct CollisionComponent
    {
        public string Tag;
        public float Radius;
    }
}
