using UnityEngine;

namespace Self.Game.Components
{
    public struct SpriteComponent 
    {
        public Sprite Sprite;
    }
}
